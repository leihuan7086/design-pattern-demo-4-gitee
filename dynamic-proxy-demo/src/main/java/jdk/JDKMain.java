package jdk;

import java.lang.reflect.Proxy;

public class JDKMain {
    public static void main(String[] args) {
        Object proxyInstance = Proxy.newProxyInstance(
                JdkExampleImpl.class.getClassLoader(),
                JdkExampleImpl.class.getInterfaces(),
                new JdkProxy(new JdkExampleImpl()));

        // proxyJdkExample的类型是实现了JdkExample接口的新类型"$Proxy+数字"
        JdkExample proxyJdkExample = (JdkExample) proxyInstance;
        System.out.println("proxyJdkExample: " + proxyJdkExample.getClass().getName());

        proxyJdkExample.methodOne();
    }
}
